package com.example.lab3;

import androidx.appcompat.app.AppCompatActivity;

import android.media.Image;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    Random random = new Random();
    Button button;
    boolean start = false;
    int lives = 0;
    int score = 0;
    ImageView image;
    ImageView image2;
    ImageView image3;
    ImageView image4;
    ImageView image5;
    TextView liveText;
    TextView ScoreText;
    int positionTideX;
    int positionTideY;
    int positionTideX2;
    int positionTideY2;
    int positionTideX3;
    int positionTideY3;
    int positionTideX4;
    int positionTideY4;
    int positionTideX5;
    int positionTideY5;
    MediaPlayer hit, thronehit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        liveText = (TextView) findViewById(R.id.textView);
        liveText.setText("Lives: " + String.valueOf(lives));
        ScoreText = (TextView) findViewById((R.id.textView2));
        ScoreText.setText("Score: " + String.valueOf(score));
        hit = MediaPlayer.create(this, R.raw.hit);
        thronehit = MediaPlayer.create(this, R.raw.thronehit);
        image = (ImageView) findViewById(R.id.imageView);
        image2 = (ImageView) findViewById(R.id.imageView2);
        image3 = (ImageView) findViewById(R.id.imageView4);
        image4 = (ImageView) findViewById(R.id.imageView5);
        image5 = (ImageView) findViewById(R.id.imageView6);
        button = (Button) findViewById(R.id.button);
    }
    public void ImageClickEvent(View view) {
        if(start){
            hit.start();
            score++;
            positionTideX = random.nextInt(800 - 0) + 1;
            positionTideY = random.nextInt(1400 - 0) + 1;
            ScoreText.setText("Score: " + String.valueOf(score));
        }
    }
    public void ImageClickEvent2(View view) {
        if(start) {
            hit.start();
            score++;
            positionTideX2 = random.nextInt(800 - 0) + 1;
            positionTideY2 = random.nextInt(1400 - 0) + 1;
            ScoreText.setText("Score: " + String.valueOf(score));
        }
    }
    public void ImageClickEvent4(View view) {
        if(start) {
            hit.start();
            score++;
            positionTideX3 = random.nextInt(800 - 0) + 1;
            positionTideY3 = random.nextInt(1400 - 0) + 1;
            ScoreText.setText("Score: " + String.valueOf(score));
        }
    }
    public void ImageClickEvent5(View view) {
        if(start) {
            hit.start();
            score++;
            positionTideX4 = random.nextInt(800 - 0) + 1;
            positionTideY4 = random.nextInt(1400 - 0) + 1;
            ScoreText.setText("Score: " + String.valueOf(score));
        }
    }
    public void ImageClickEvent6(View view) {
        if(start) {
            hit.start();
            score++;
            positionTideX5 = random.nextInt(800 - 0) + 1;
            positionTideY5 = random.nextInt(1400 - 0) + 1;
            ScoreText.setText("Score: " + String.valueOf(score));
        }
    }
    public void OnClickStart(View view){
        start = true;
        lives = 3;
        score = 0;
        ScoreText.setText("Score: " + String.valueOf(score));
        button.setX(-450);
        button.setY(-450);
        new Thread(new Runnable() {
            @Override
            public void run() {
                positionTideX = random.nextInt(800 - 0) + 1;
                positionTideY = random.nextInt(1400 - 0) + 1;
                while(true){
                    if(positionTideX < 450 && positionTideY < 850) {
                        positionTideY++;
                        positionTideX++;
                    } else if(positionTideX > 450 && positionTideY > 850) {
                        positionTideY--;
                        positionTideX--;
                    } else if (positionTideX < 450 && positionTideY > 850){
                        positionTideY--;
                        positionTideX++;
                    } else if (positionTideX > 450 && positionTideY < 850){
                        positionTideY++;
                        positionTideX--;
                    } else if (positionTideX == 450 && positionTideY < 850){
                        positionTideY++;
                    } else if (positionTideX == 450 && positionTideY > 850){
                        positionTideY--;
                    } else if (positionTideX < 450 && positionTideY == 850){
                        positionTideX++;
                    } else if (positionTideX > 450 && positionTideY == 850){
                        positionTideY--;
                    }
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            image.setX(positionTideX);
                            image.setY(positionTideY);
                            liveText.setText("Lives: " + String.valueOf(lives));
                        }
                    });
                    try {
                        Thread.sleep(10);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    if(positionTideX == 450 && positionTideY == 850){
                        thronehit.start();
                        lives--;
                        positionTideX = random.nextInt(800 - 0) + 1;
                        positionTideY = random.nextInt(1400 - 0) + 1;
                    }
                    if(lives == 0){
                        button.setY(850);
                        button.setX(450);
                        start = false;
                        break;
                    }
                }
            }
        }).start();
        new Thread(new Runnable() {
            @Override
            public void run() {
                positionTideX2 = random.nextInt(800 - 0) + 1;
                positionTideY2 = random.nextInt(1400 - 0) + 1;
                while(true){
                    if(positionTideX2 < 450 && positionTideY2 < 850) {
                        positionTideY2++;
                        positionTideX2++;
                    } else if(positionTideX2 > 450 && positionTideY2 > 850) {
                        positionTideY2--;
                        positionTideX2--;
                    } else if (positionTideX2 < 450 && positionTideY2 > 850){
                        positionTideY2--;
                        positionTideX2++;
                    } else if (positionTideX2 > 450 && positionTideY2 < 850){
                        positionTideY2++;
                        positionTideX2--;
                    } else if (positionTideX2 == 450 && positionTideY2 < 850){
                        positionTideY2++;
                    } else if (positionTideX2 == 450 && positionTideY2 > 850){
                        positionTideY2--;
                    } else if (positionTideX2 < 450 && positionTideY2 == 850){
                        positionTideX2++;
                    } else if (positionTideX2 > 450 && positionTideY2 == 850){
                        positionTideY2--;
                    }

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            image2.setX(positionTideX2);
                            image2.setY(positionTideY2);
                            liveText.setText("Lives: " + String.valueOf(lives));
                        }
                    });
                    try {
                        Thread.sleep(15);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    if(positionTideX2 == 450 && positionTideY2 == 850){
                        thronehit.start();
                        lives--;
                        positionTideX2 = random.nextInt(800 - 0) + 1;
                        positionTideY2 = random.nextInt(1400 - 0) + 1;
                    }
                    if(lives == 0){
                        button.setY(850);
                        button.setX(450);
                        break;
                    }
                }
            }
        }).start();
        new Thread(new Runnable() {
            @Override
            public void run() {
                positionTideX3 = random.nextInt(800 - 0) + 1;
                positionTideY3 = random.nextInt(1400 - 0) + 1;
                while(true){
                    if(positionTideX3 < 450 && positionTideY3 < 850) {
                        positionTideY3++;
                        positionTideX3++;
                    } else if(positionTideX3 > 450 && positionTideY3 > 850) {
                        positionTideY3--;
                        positionTideX3--;
                    } else if (positionTideX3 < 450 && positionTideY3 > 850){
                        positionTideY3--;
                        positionTideX3++;
                    } else if (positionTideX3 > 450 && positionTideY3 < 850){
                        positionTideY3++;
                        positionTideX3--;
                    } else if (positionTideX3 == 450 && positionTideY3 < 850){
                        positionTideY3++;
                    } else if (positionTideX3 == 450 && positionTideY3 > 850){
                        positionTideY3--;
                    } else if (positionTideX3 < 450 && positionTideY3 == 850){
                        positionTideX3++;
                    } else if (positionTideX3 > 450 && positionTideY3 == 850){
                        positionTideY3--;
                    }

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            image3.setX(positionTideX3);
                            image3.setY(positionTideY3);
                            liveText.setText("Lives: " + String.valueOf(lives));
                        }
                    });
                    try {
                        Thread.sleep(15);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    if(positionTideX3 == 450 && positionTideY3 == 850){
                        thronehit.start();
                        lives--;
                        positionTideX3 = random.nextInt(800 - 0) + 1;
                        positionTideY3 = random.nextInt(1400 - 0) + 1;
                    }
                    if(lives == 0){
                        button.setY(850);
                        button.setX(450);
                        break;
                    }
                }
            }
        }).start();
        new Thread(new Runnable() {
            @Override
            public void run() {
                positionTideX4 = random.nextInt(800 - 0) + 1;
                positionTideY4 = random.nextInt(1400 - 0) + 1;
                while(true){
                    if(positionTideX4 < 450 && positionTideY4 < 850) {
                        positionTideY4++;
                        positionTideX4++;
                    } else if(positionTideX4 > 450 && positionTideY4 > 850) {
                        positionTideY4--;
                        positionTideX4--;
                    } else if (positionTideX4 < 450 && positionTideY4 > 850){
                        positionTideY4--;
                        positionTideX4++;
                    } else if (positionTideX4 > 450 && positionTideY4 < 850){
                        positionTideY4++;
                        positionTideX4--;
                    } else if (positionTideX4 == 450 && positionTideY4 < 850){
                        positionTideY4++;
                    } else if (positionTideX4 == 450 && positionTideY4 > 850){
                        positionTideY4--;
                    } else if (positionTideX4 < 450 && positionTideY4 == 850){
                        positionTideX4++;
                    } else if (positionTideX4 > 450 && positionTideY4 == 850){
                        positionTideY4--;
                    }

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            image4.setX(positionTideX4);
                            image4.setY(positionTideY4);
                            liveText.setText("Lives: " + String.valueOf(lives));
                        }
                    });
                    try {
                        Thread.sleep(25);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    if(positionTideX4 == 450 && positionTideY4 == 850){
                        thronehit.start();
                        lives--;
                        positionTideX4 = random.nextInt(800 - 0) + 1;
                        positionTideY4 = random.nextInt(1400 - 0) + 1;
                    }
                    if(lives == 0){
                        button.setY(850);
                        button.setX(450);
                        break;
                    }
                }
            }
        }).start();
        new Thread(new Runnable() {
            @Override
            public void run() {
                positionTideX5 = random.nextInt(800 - 0) + 1;
                positionTideY5 = random.nextInt(1400 - 0) + 1;
                while(true){
                    if(positionTideX5 < 450 && positionTideY5 < 850) {
                        positionTideY5++;
                        positionTideX5++;
                    } else if(positionTideX5 > 450 && positionTideY5 > 850) {
                        positionTideY5--;
                        positionTideX5--;
                    } else if (positionTideX5 < 450 && positionTideY5 > 850){
                        positionTideY5--;
                        positionTideX5++;
                    } else if (positionTideX5 > 450 && positionTideY5 < 850){
                        positionTideY5++;
                        positionTideX5--;
                    } else if (positionTideX5 == 450 && positionTideY5 < 850){
                        positionTideY5++;
                    } else if (positionTideX5 == 450 && positionTideY5 > 850){
                        positionTideY5--;
                    } else if (positionTideX5 < 450 && positionTideY5 == 850){
                        positionTideX5++;
                    } else if (positionTideX5 > 450 && positionTideY5 == 850){
                        positionTideY5--;
                    }

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            image5.setX(positionTideX5);
                            image5.setY(positionTideY5);
                            liveText.setText("Lives: " + String.valueOf(lives));
                        }
                    });
                    try {
                        Thread.sleep(20);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    if(positionTideX5 == 450 && positionTideY5 == 850){
                        thronehit.start();
                        lives--;
                        positionTideX5 = random.nextInt(800 - 0) + 1;
                        positionTideY5 = random.nextInt(1400 - 0) + 1;
                    }
                    if(lives == 0){
                        button.setY(850);
                        button.setX(450);
                        break;
                    }
                }
            }
        }).start();
    }
}